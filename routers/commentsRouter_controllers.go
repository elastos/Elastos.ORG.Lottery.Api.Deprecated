package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["Elastos.ORG.Lottery.Api/controllers:ChainController"] = append(beego.GlobalControllerRouter["Elastos.ORG.Lottery.Api/controllers:ChainController"],
		beego.ControllerComments{
			Method:           "Post",
			Router:           `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Params:           nil})

	beego.GlobalControllerRouter["Elastos.ORG.Lottery.Api/controllers:ValidationCodeController"] = append(beego.GlobalControllerRouter["Elastos.ORG.Lottery.Api/controllers:ValidationCodeController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           `/:secret`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Params:           nil})

}
