package controllers

import (
	"Elastos.ORG.Lottery.Api/conf"
	"Elastos.ORG.Lottery.Api/db"
	"Elastos.ORG.Lottery.Api/models"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"github.com/astaxie/beego"
	"io/ioutil"
	"net/http"
	"strings"
)

type ChainController struct {
	beego.Controller
}

// @Title Create
// @Description create object
// @Param	body		body 	models.Object	true		"The object content"
// @Success 200 {string} models.Object.Id
// @Failure 403 body is empty
// @router / [post]
func (this *ChainController) Post() {
	var reqBody map[string]interface{}
	err := json.Unmarshal(this.Ctx.Input.RequestBody, &reqBody)
	if err != nil {
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: "not a valid json"}
		this.ServeJSON()
		return
	}
	reqData := reqBody["Data"].(string)
	if strings.Index(reqData,beego.AppConfig.String("writeChainSecret")) == -1{
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: "not a valid secret"}
		this.ServeJSON()
		return
	}
	if reqData == "" {
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: "can not find request json key `Data`"}
		this.ServeJSON()
		return
	}
	sha := sha256.Sum256([]byte(reqData))
	memo_hash := "chinajoy" + hex.EncodeToString(sha[:])

	l, err := db.Dia.Query("select * from elastos_addresses where status = 1")
	if err != nil {
		beego.Error(err)
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: err.Error()}
		this.ServeJSON()
		return
	}
	v := l.Front().Value.(map[string]string)
	body := `{
			"Action":"transfer",
			"Version":"1.0.0",
			"Data":
				{"senderAddr":"` + v["publicAddr"] + `",
   				 "senderPrivateKey":"` + v["privKey"] + `",
				 "memo":"` + memo_hash + `",
				 "receiver":` + `[{"address":"` + conf.Config.MemoReceiveAddr + `","amount":"0.00000001"}]` + `
				}
			}`
	beego.Info("request info ", body)
	r := strings.NewReader(body)
	rsp, err := http.Post(conf.Config.ChainApi.SendTransfer, "application/json", r)
	if err != nil {
		beego.Error(err)
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: err.Error()}
		this.ServeJSON()
		return
	}
	bytes, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		beego.Error(err)
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: err.Error()}
		this.ServeJSON()
		return
	}
	var ret map[string]interface{}
	beego.Debug("ret Msg : %s \n", string(bytes))
	err = json.Unmarshal(bytes, &ret)
	if err != nil {
		beego.Error(err)
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: err.Error()}
		this.ServeJSON()
		return
	}
	var txid string
	if ret["error"].(float64) != 0 {
		beego.Error("send fee Error")
	} else {
		txid = ret["result"].(string)
		// update current active address
		_, err = db.Dia.Exec(" update elastos_addresses set status = 0 ,  spendTime = spendTime + 1 where status = 1")
		if err != nil {
			beego.Error(err)
			this.Data["json"] = models.RetMsg{Desc: err.Error(), Error: -2}
			this.ServeJSON()
			return
		}
		id, err := db.Dia.Exec(" update elastos_addresses set status = 1 where id=" + v["id"] + "+1")
		if err != nil {
			beego.Error(err)
			this.Data["json"] = models.RetMsg{Desc: err.Error(), Error: -2}
			this.ServeJSON()
			return
		} else if id == 0 {
			l, _ := db.Dia.Query("select id from elastos_addresses limit 1")

			if l != nil && l.Len() > 0 {
				_, err := db.Dia.Exec(" update elastos_addresses set status = 1 where id = " + l.Front().Value.(map[string]string)["id"])
				if err != nil {
					beego.Error(err)
					this.Data["json"] = models.RetMsg{Desc: err.Error(), Error: -2}
					this.ServeJSON()
					return
				}
			}
		}
	}

	_, err = db.Dia.Exec("insert into elastos_transfer(wallet_addr,amount,raw_memo,hash_memo,status,txid) values" +
		"('" + conf.Config.MemoReceiveAddr + "','0.00000001','" + reqData + "','" + memo_hash + "',1,'" + txid + "')")
	if err != nil {
		this.Data["json"] = models.RetMsg{Result: "", Error: -1, Desc: err.Error()}
		this.ServeJSON()
		return
	}

	this.Data["json"] = models.RetMsg{Result: txid, Error: 0, Desc: "SUCCESS"}
	this.ServeJSON()
}
