
all:
	go get github.com/astaxie/beego
	go get github.com/go-sql-driver/mysql
	go build -o api main.go

format:
	go fmt ./...

run:
	nohup ./api &

clean:
	rm -rf *.8 *.o *.out *.6
