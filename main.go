package main

import (
	"Elastos.ORG.Lottery.Api/jobs"
	_ "Elastos.ORG.Lottery.Api/routers"
	"github.com/astaxie/beego"
)

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.Run()
}

func init() {
	beego.SetLogger("file", `{"filename":"lottery.log"}`)
	go jobs.Process()
	go jobs.InitAddr()
}
