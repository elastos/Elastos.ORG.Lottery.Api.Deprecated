package models

import (
	"Elastos.ORG.Lottery.Api/db"
	"sync"
	"time"
)

var (
	SUCCESS              = RetMsg{"", 0, "success"}
	FAIL_INVALID_PARAM   = RetMsg{"", -1, "invalid param"}
	FAIL_INTERNAL_ERROR  = RetMsg{"", -2, "internal error"}
	UpdateTimestamp      = time.Now().Unix()
	UpdatetTimestampLock sync.RWMutex
)

type RetMsg struct {
	Result interface{}
	Error  int
	Desc   string
}

func GetVldCode(secret string) (RetMsg, error) {
	l, err := db.Dia.Query("select vldCode from elastos_info where secretCode =  '" + secret + "'")
	if err != nil || l.Len() == 0 {
		return FAIL_INVALID_PARAM, err
	}

	ret := SUCCESS
	m := make(map[string]interface{})
	m["code"] = l.Front().Value.(map[string]string)["vldCode"]
	UpdatetTimestampLock.RLock()
	defer UpdatetTimestampLock.RUnlock()
	m["timeLeft"] = 60 - (time.Now().Unix() - UpdateTimestamp)
	ret.Result = m
	return ret, nil
}
